const withLess = require('@zeit/next-less');
const withSass = require('@zeit/next-sass');
const withCss = require('@zeit/next-css');
const withImages = require('next-images');
const withPlugins = require('next-compose-plugins');
const path = require('path');
const generateTheme = require('next-dynamic-antd-theme/plugin');
const { nextI18NextRewrites } = require('next-i18next/rewrites');

const withAntdTheme = generateTheme({
  antDir: path.join(__dirname, './node_modules/antd'),
  stylesDir: path.join(__dirname, './src/themes/styles'),
  varFile: path.join(__dirname, './src/themes/styles/variables.less'),
  outputFilePath: path.join(__dirname, './.next/static/color.less'),
  lessFilePath: `/_next/static/color.less`,
  lessJSPath: 'https://cdnjs.cloudflare.com/ajax/libs/less.js/3.11.3/less.min.js'
});

withAntd = (nextConfig = {}) => {
  return Object.assign({}, nextConfig, {
    ...nextConfig,
    lessLoaderOptions: {
      ...nextConfig.lessLoaderOptions,
      javascriptEnabled: true
    },
    webpack(config, options) {
      if (config.externals) {
        const includes = [/antd/];
        config.externals = config.externals.map((external) => {
          if (typeof external !== 'function') return external;
          return (ctx, req, cb) => {
            return includes.find((include) =>
              req.startsWith('.') ? include.test(path.resolve(ctx, req)) : include.test(req)
            )
              ? cb()
              : external(ctx, req, cb);
          };
        });
      }
      return typeof nextConfig.webpack === 'function'
        ? nextConfig.webpack(config, options)
        : config;
    }
  });
};

//https://github.com/isaachinman/next-i18next#5-locale-subpaths
const localeSubpaths = {
  vi: 'vi',
  en: 'en'
};

const configNext = {
  pageExtensions: ['page.jsx', 'page.js', 'page.ts', 'page.tsx'],
  serverRuntimeConfig: {},
  publicRuntimeConfig: {
    localeSubpaths
  },
  rewrites: async () => [
    ...(await nextI18NextRewrites(localeSubpaths)),
    {
      source: '/cms',
      destination: '/cms'
    },
    {
      source: '/cms/:path*',
      destination: '/cms/:path*'
    },
    {
      source: '/auth',
      destination: '/auth'
    },
    {
      source: '/auth/:path*',
      destination: '/auth/:path*'
    },
    {
      source: '/',
      destination: '/frontend/'
    },
    {
      source: '/:path*',
      destination: '/frontend/:path*'
    }
  ]
};

module.exports = withPlugins([
  withLess(withAntdTheme(withAntd(configNext))),
  withSass,
  withCss,
  withImages
]);
