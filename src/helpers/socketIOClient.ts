import SocketIOClient from 'socket.io-client';

const socketIOClient = (token?: string) =>
  SocketIOClient(process.env.REACT_APP_SERVER_SOCKET_IO || 'ws://localhost:4000', {
    transports: ['polling'],
    query: { token }
  });

export default socketIOClient;
