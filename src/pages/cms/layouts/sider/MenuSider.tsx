import { HomeOutlined, TeamOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import { Link, router, useTranslation } from 'pages/i18n';
import { useState } from 'react';

const { SubMenu, Item } = Menu;

export interface ConfigMenu {
  icon?: JSX.Element;
  title: string;
  path: string;
  children?: ConfigMenu[];
}

function MenuSider() {
  const { t } = useTranslation();
  const [configMenus] = useState<ConfigMenu[]>([
    {
      icon: <HomeOutlined />,
      title: 'home',
      path: '/cms'
    },
    {
      icon: <TeamOutlined />,
      title: 'user',
      path: '/cms/user'
    }
  ]);
  const findOpenKeys = (
    configMenus: ConfigMenu[],
    withOpenKeys: string[] = [],
    menuSelected: ConfigMenu[] = []
  ): { openKeys: string[]; menuSelected: ConfigMenu[] } => {
    if (!configMenus.length) {
      return { openKeys: withOpenKeys, menuSelected };
    }
    const newWithOpenKeys: string[] = [...withOpenKeys];
    const newConfigMenus: ConfigMenu[] = [];
    const newMenuSelected: ConfigMenu[] = [...menuSelected];
    configMenus.map((configMenu) => {
      if (configMenu.path === router.pathname) {
        newMenuSelected.push(configMenu);
      }
      if (configMenu.children?.length) {
        configMenu.children.map((config: ConfigMenu) => {
          newConfigMenus.push(config);
          if (config.path === router.pathname) {
            newWithOpenKeys.push(configMenu.path);
          }
          return config;
        });
      }
      return configMenu;
    });
    return findOpenKeys(newConfigMenus, newWithOpenKeys, newMenuSelected);
  };
  const [findOpenConfigMenus] = useState(findOpenKeys(configMenus));

  const renderItem = (configMenu: ConfigMenu) => {
    if (configMenu?.children?.length) {
      return (
        <SubMenu icon={configMenu.icon} key={configMenu.path} title={t(configMenu.title)}>
          {configMenu.children.map((config) => {
            return renderItem(config);
          })}
        </SubMenu>
      );
    } else {
      return (
        <Item icon={configMenu.icon} key={configMenu.path}>
          <Link href={configMenu.path}>{t(configMenu.title)}</Link>
        </Item>
      );
    }
  };

  return (
    <Menu
      className="auto-hidden-scroll"
      style={{
        height: '100%',
        width: '100%',
        overflowY: 'auto',
        overflowX: 'hidden',
        borderRight: 'none'
      }}
      mode="inline"
      defaultSelectedKeys={[router.pathname]}
      defaultOpenKeys={findOpenConfigMenus.openKeys}>
      {configMenus.map((configMenu) => renderItem(configMenu))}
    </Menu>
  );
}

export default MenuSider;
