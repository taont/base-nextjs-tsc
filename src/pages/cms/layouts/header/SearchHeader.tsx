import { SearchOutlined } from '@ant-design/icons';
import { useDebounceEffect } from 'ahooks';
import { Button, Card, Drawer, Dropdown, Empty, Grid, Input } from 'antd';
import Loading from 'pages/components/base/Loading';
import { useTranslation } from 'pages/i18n';
import { useEffect, useRef, useState } from 'react';

const { useBreakpoint } = Grid;

const styleDefault = {
  width: 300,
  maxHeight: 300
};

interface Props {
  width?: number;
  maxHeight?: number;
}

function SearchHeader(props: Props) {
  const { width, maxHeight } = props;
  const { t } = useTranslation();
  const screens = useBreakpoint();
  const [showDrawerSearch, setShowDrawerSearch] = useState(false);
  const [inputSearch, setInputSearch] = useState<Input | null>(null);
  const [value, setValue] = useState('');
  const [loading, setLoading] = useState(false);
  const [dataSearch, setDataSearch] = useState<boolean>();

  useEffect(() => {
    const inputSearchFocus = () => {
      if (showDrawerSearch && inputSearch) {
        inputSearch.focus();
      }
    };
    inputSearchFocus();
  }, [showDrawerSearch, inputSearch]);

  const timeOutRef = useRef<number>(0);
  useDebounceEffect(
    () => {
      const fetchData = async () => {
        if (!value) {
          return;
        }
        setLoading(true);
        await new Promise((resolve) => {
          setDataSearch(undefined);
          timeOutRef.current = window.setTimeout(() => {
            setDataSearch(false);
            resolve(true);
          }, 3000);
        });
        setLoading(false);
      };
      fetchData();
      return () => clearTimeout(timeOutRef.current);
    },
    [value],
    {
      wait: 300
    }
  );

  const renderResult = (style: { width: string | number; maxHeight: string | number }) => {
    if (!loading && value !== '' && dataSearch !== undefined) {
      return (
        <Card
          bodyStyle={{ padding: '.5em' }}
          style={{
            width: style.width,
            maxHeight: style.maxHeight,
            overflowY: 'auto',
            padding: '0'
          }}>
          <>
            {dataSearch ? (
              <div>Data</div>
            ) : (
              <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('no_result')} />
            )}
          </>
        </Card>
      );
    } else {
      return <></>;
    }
  };

  const renderBoxSearch = (style: { width: string | number; maxHeight: string | number }) => {
    return (
      <Dropdown overlay={renderResult(style)} trigger={['click']}>
        <Input
          ref={(input) => {
            setInputSearch(input);
          }}
          style={{ width: style.width }}
          placeholder={t('input_search')}
          suffix={loading ? <Loading /> : <SearchOutlined />}
          onChange={(e) => setValue(e.target.value)}
        />
      </Dropdown>
    );
  };

  if (screens.xs) {
    return (
      <>
        <Button
          onClick={() => setShowDrawerSearch(true)}
          icon={<SearchOutlined />}
          type="text"
          shape="circle"
        />
        <Drawer
          title={undefined}
          placement="top"
          closable={false}
          onClose={() => setShowDrawerSearch(false)}
          visible={showDrawerSearch}
          height="auto">
          {renderBoxSearch({
            width: '100%',
            maxHeight: maxHeight || styleDefault.maxHeight
          })}
        </Drawer>
      </>
    );
  } else {
    return (
      <>
        {renderBoxSearch({
          width: width || styleDefault.width,
          maxHeight: maxHeight || styleDefault.maxHeight
        })}
      </>
    );
  }
}

export default SearchHeader;
