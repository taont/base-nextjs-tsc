import { SettingOutlined } from '@ant-design/icons';
import { Button, Drawer, Row, Space, Switch } from 'antd';
import useTheme from 'hooks/base/theme/useTheme';
import ChangeLanguage from 'pages/components/base/ChangeLocale';
import CirclePickerCustom from 'pages/components/base/CirclePickerCustom';
import { useTranslation } from 'pages/i18n';
import React, { useState } from 'react';
import { ColorResult } from 'react-color';

function SettingHeader() {
  const { t } = useTranslation();
  const { theme, setTheme } = useTheme();
  const [showSetting, setShowSetting] = useState<boolean>(false);
  const toggleSetting = (status?: boolean) => {
    setShowSetting(status !== undefined ? status : !showSetting);
  };
  return (
    <>
      <Button
        onClick={() => toggleSetting()}
        type="text"
        shape="circle"
        icon={<SettingOutlined />}
      />
      <Drawer
        title={t('setting')}
        placement="right"
        onClose={() => toggleSetting()}
        visible={showSetting}
        width="280">
        <Space direction="vertical" style={{ width: '100%' }}>
          <Row gutter={[8, 8]} justify="space-between" align="middle">
            {t('language')}
            <ChangeLanguage />
          </Row>
          <Row gutter={[8, 8]} justify="space-between" align="middle">
            {t('theme:dark_mode')}
            <Switch
              onChange={() => {
                theme.name === 'dark' ? setTheme({ name: 'default' }) : setTheme({ name: 'dark' });
              }}
              checked={theme.name === 'dark'}
              size="small"
            />
          </Row>
          <Row gutter={[8, 8]} justify="space-between" align="middle">
            <CirclePickerCustom
              onChange={(color: ColorResult) => {
                setTheme({ variables: { '@primary-color': color.hex } });
              }}
              color={theme.variables && theme.variables['@primary-color']}
            />
          </Row>
        </Space>
      </Drawer>
    </>
  );
}

export default SettingHeader;
