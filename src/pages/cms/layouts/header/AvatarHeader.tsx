import { UserOutlined } from '@ant-design/icons';
import { Button, Dropdown, Menu } from 'antd';
import useAuth from 'hooks/base/auth/useAuth';
import { useTranslation } from 'pages/i18n';
import React from 'react';

const MenuAccount = () => {
  const { t } = useTranslation();
  const { logout } = useAuth();
  return (
    <Menu selectable={false}>
      <Menu.Item>{t('account')}</Menu.Item>
      <Menu.Item onClick={() => logout()}>{t('logout')}</Menu.Item>
    </Menu>
  );
};

function AvatarHeader() {
  return (
    <Dropdown overlay={<MenuAccount />} placement="bottomRight" arrow>
      <Button type="text" shape="circle" icon={<UserOutlined />} />
    </Dropdown>
  );
}

export default AvatarHeader;
