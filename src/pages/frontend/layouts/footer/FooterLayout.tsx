import { Card, Layout } from 'antd';
import { BasicProps } from 'antd/lib/layout/layout';

const { Footer } = Layout;

type Props = BasicProps;

function FooterLayout(props: Props) {
  const { ...reset } = props;
  return (
    <Footer {...reset}>
      <Card>Footer</Card>
    </Footer>
  );
}

export default FooterLayout;
