import { Layout, Row } from 'antd';
import { BasicProps } from 'antd/lib/layout/layout';
import LogoHeader from 'pages/frontend/layouts/header/LogoHeader';

import MenuHeader from './MenuHeader';

const { Header } = Layout;

type Props = BasicProps;

function HeaderLayout(props: Props) {
  const { ...reset } = props;
  return (
    <Header id="main-header" {...reset}>
      <Row gutter={[8, 8]} justify="space-between" align="middle" style={{ margin: 0 }}>
        <LogoHeader />
        <MenuHeader
          style={{
            height: reset?.style?.height || '4em',
            lineHeight: reset?.style?.height || '4em'
          }}
        />
      </Row>
    </Header>
  );
}

export default HeaderLayout;
