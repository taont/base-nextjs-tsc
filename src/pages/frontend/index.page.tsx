import Container from 'pages/components/base/Container';
import MasterLayout from 'pages/frontend/layouts/MasterLayout';
import { Link } from 'pages/i18n';

function IndexPage() {
  return (
    <Container style={{ position: 'relative' }}>
      <MasterLayout>
        <Link href="/product">Product</Link>
      </MasterLayout>
    </Container>
  );
}

export default IndexPage;
