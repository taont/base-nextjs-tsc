import useAuth from 'hooks/base/auth/useAuth';
import { router } from 'pages/i18n';
import React from 'react';

interface Config {
  permissions: Array<string | number>;
  redirect?: string;
}

// eslint-disable-next-line react/display-name
const withPermission = (WrappedComponent) => (config: Config) => () => {
  const { auth } = useAuth();
  if (auth.token) {
    return <WrappedComponent />;
  }
  if (config?.redirect) {
    try {
      router.push(config.redirect);
      // eslint-disable-next-line no-empty
    } catch (error) {}
  }
  return <></>;
};

export default withPermission;
