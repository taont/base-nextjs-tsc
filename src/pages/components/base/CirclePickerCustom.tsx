import { CirclePicker, CirclePickerProps } from 'react-color';

type Props = CirclePickerProps;

function CirclePickerCustom(props: Props) {
  return (
    <CirclePicker
      {...props.width}
      onChange={props.onChange}
      color={props.color}
      colors={
        props.colors || [
          '#f44336',
          '#e91e63',
          '#9c27b0',
          '#673ab7',
          '#3f51b5',
          '#1890ff',
          '#03a9f4',
          '#00bcd4',
          '#009688',
          '#4caf50',
          '#8bc34a',
          '#cddc39',
          '#ffeb3b',
          '#ffc107',
          '#ff9800',
          '#ff5722',
          '#795548',
          '#607d8b'
        ]
      }
    />
  );
}

export default CirclePickerCustom;
