import { Loading3QuartersOutlined } from '@ant-design/icons';
import useTheme from 'hooks/base/theme/useTheme';
import ElementCenter from 'pages/components/base/ElementCenter';
import React from 'react';

interface Props {
  loading?: boolean;
  size?: number | string;
  color?: string;
}

const Loading = (props: Props) => {
  const { loading, size, color } = props;
  const { theme } = useTheme();
  return (
    <>
      {loading !== false && (
        <ElementCenter>
          <Loading3QuartersOutlined
            spin
            style={{ fontSize: size || '100%', color: color || theme.variables['@primary-color'] }}
          />
        </ElementCenter>
      )}
    </>
  );
};

export default Loading;
