import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import localStorageHelper, { KeyStorage } from 'helpers/localStorage';
import _ from 'lodash';

export interface User {
  id: number;
  name: string;
}
export interface LocalAuth {
  token: string | null;
  user?: User;
}
const localAuth = localStorageHelper.getObject(KeyStorage.AUTH) as LocalAuth;

const initialState: LocalAuth = localAuth;

const auth = createSlice({
  name: 'auth',
  initialState: initialState,
  reducers: {
    changeAuth: (state: LocalAuth, action: PayloadAction<LocalAuth>) => {
      state = _.merge(state, action.payload);
    }
  }
});

const { reducer, actions } = auth;
export const { changeAuth } = actions;
export default reducer;
