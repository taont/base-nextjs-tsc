import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import _ from 'lodash';

type Status = 'start' | 'complete' | 'error';

interface NProgressState {
  status: Status;
  loading: boolean;
}

const initialState: NProgressState = { status: 'complete', loading: false };
const nprogress = createSlice({
  name: 'nprogress',
  initialState: initialState,
  reducers: {
    changeNProgress: (state: NProgressState, action: PayloadAction<NProgressState>) => {
      state = _.merge(state, action.payload);
    }
  }
});

const { reducer, actions } = nprogress;
export const { changeNProgress } = actions;
export default reducer;
