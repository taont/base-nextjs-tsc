import locale from './locale/reducer';
import theme from './theme/reducer';
import uiCms from './ui-cms/reducer';
import auth from './auth/reducer';
import nprogress from './nprogress/reducer';

export default { theme, locale, uiCms, auth, nprogress };
