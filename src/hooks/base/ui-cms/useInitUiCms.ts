import localStorageHelper, { KeyStorage } from 'helpers/localStorage';
import { useEffect } from 'react';
import useUiCms from './useUiCms';

function useInitUiCms(): void {
  const { uiCms } = useUiCms();
  useEffect(() => {
    localStorageHelper.setObject(KeyStorage.UI_CMS, uiCms);
  }, [uiCms]);
}

export default useInitUiCms;
